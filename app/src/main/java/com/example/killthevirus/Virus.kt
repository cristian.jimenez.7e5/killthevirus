package com.example.killthevirus

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.coroutines.*

class Virus(
    var virusImage: ImageView,
    var velocity: MutableList<Float>
) {
    private var job: Job? = null

    fun startMoving() {
        job = CoroutineScope(Dispatchers.Main).launch {
            while (true) {
                delay(100)
                virusImage.x += velocity[0]
                virusImage.y += velocity[1]

                if (virusImage.x <= 0 || virusImage.x + virusImage.width >= (virusImage.parent as ViewGroup).width) {
                    velocity[0] = -velocity[0]
                }

                if (virusImage.y <= 0 || virusImage.y + virusImage.height >= (virusImage.parent as ViewGroup).height) {
                    velocity[1] = -velocity[1]
                }
            }
        }
    }

    fun handleVirusClick(newView: ImageView) {
        newView.setImageResource(R.drawable.mad_virus)
        CoroutineScope(Dispatchers.Main).launch {
            this@Virus.stopMoving()
            delay(2000)
            newView.visibility = View.GONE
        }
    }

    fun stopMoving() {
        job?.cancel()
    }
}