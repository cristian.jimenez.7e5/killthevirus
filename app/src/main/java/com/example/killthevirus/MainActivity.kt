package com.example.killthevirus

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.example.killthevirus.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val listOfVirus = mutableListOf<Virus>()
    private var numberOfVirusesLeft = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.spreadTheDisease.setOnClickListener{
            startGame()
            CoroutineScope(Dispatchers.Main).launch {
                delay(2000)
                it.visibility = View.GONE
            }
        }
    }

    fun startGame(){
        createTheViruses()
    }

    private fun createTheViruses(){
        val numberOfVirus = (4..9).random()
        numberOfVirusesLeft = numberOfVirus
        for(i in 1..numberOfVirus){

            val newView = ImageView(applicationContext)
            binding.gameLayout.addView(newView)
            newView.layoutParams.height = 160
            newView.layoutParams.width = 160
            val initialPosition = getRandomInitialPosition()
            newView.x = initialPosition[0]
            newView.y = initialPosition[1]
            newView.setImageResource(R.drawable.happy_virus)

//            Log.d("MainActivity", "Added new ImageView at x=${newView.x}, y=${newView.y}")
//            Log.d("MainActivity", "GameLayout child count: ${binding.gameLayout.childCount}")

            val virus = Virus(newView, getRandomVelocity())
            listOfVirus.add(virus)


            newView.setOnClickListener {
                virus.handleVirusClick(newView)
                numberOfVirusesLeft--
                if (numberOfVirusesLeft == 0) {
                    binding.gameLayout.setBackgroundColor(Color.GREEN) // Change the background color to blue
                    Toast.makeText(applicationContext, "All viruses have been eliminated!", Toast.LENGTH_LONG).show()
                }
            }
        }
        moveViruses()
    }

    private fun getRandomInitialPosition(): List<Float> {
        val displayMetrics = resources.displayMetrics
        val screenWidth = displayMetrics.widthPixels
        val screenHeight = displayMetrics.heightPixels

        val x = (50..screenWidth-100).random()
        val y = (50..screenHeight-150).random()

        return listOf(x.toFloat(), y.toFloat())
    }

    private fun getRandomVelocity(): MutableList<Float>{
        val possibleSenses = listOf(-1, 1)
        val randomXVelocity = (20..40).random().toFloat()
        val randomXVelocitySense = Random.nextInt(possibleSenses.size)
        val randomYVelocity = (20..40).random().toFloat()
        val randomYVelocitySense = Random.nextInt(possibleSenses.size)

        return mutableListOf(randomXVelocity*randomXVelocitySense, randomYVelocity*randomYVelocitySense)
    }

    private fun moveViruses(){
        listOfVirus.forEach {
            it.startMoving()
        }
    }
}